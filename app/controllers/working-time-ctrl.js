'use strict';
angular
    .module('appClient')
        .controller('workingTimeCtrl', 
            ['$scope', '$log', '$http', '$routeParams', '$filter', 'moment',
                function($scope, $log, $http, $routeParams, $filter, moment) {

                    
                    $scope.date   = new Date();
                    $scope.currentDate = new Date();
                    $scope.workingTime  = null;
                    
                    const employeeSummaryTime = 'http://localhost:3000/api/v1/employees/:employeeId/summary/:date';
                    
                    $scope.getEmployee = function(){

                        // let employeeSummaryTime = 'http://localhost:3000/api/v1/employees/:employeeId/summary/:date';
                        $http.get('http://localhost:3000/api/v1/employees')
                            .then ( ( { data } ) =>  { 
                                $scope.employeeData  = $filter('filter')(data, function (d) { 
                                    console.log(parseInt($routeParams.id));
                                    return d.EmployeeID === parseInt($routeParams.id)})[0];
                                // console.log(a);
                                // let a = moment('2016-01-01').add(365, 'day').format('LL');
                                // console.log(a);
                                // $scope.currentDate = moment(new Date($scope.employeeData.Date)).format('YYYY-MM-DD');
                                $scope.currentDate = new Date($scope.employeeData.Date);
                                console.log($scope.employeeData, ' + ', $scope.currentDate);
                                // $scope.employee  = data
                                })    
                            .catch( error => { 
                                $log.error("error", error);
                            });
                        }

                    $scope.getEmployee();

                    $scope.clearDate = function(){ 
                        $scope.workingTime = null;
                    };

                    $scope.getSummaryTime = function(){

                        var employeeId = $routeParams.id
                        var date = moment(new Date($scope.currentDate)).format('YYYY-MM-DD');
                        
                        $http.get(employeeSummaryTime.replace(':employeeId', employeeId).replace(':date', date))
                            .then ( ( { data } ) =>  {
                                $scope.workingTime = data.totalTime;
                                console.info(data.totalTime);
                            })    
                            .catch( error => { 
                                $log.error("error", error);
                            });
                    }            
        }]);